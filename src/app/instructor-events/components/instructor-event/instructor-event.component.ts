import {Component, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ClientDataQueryable } from '@themost/client';
import { ErrorService, LoadingService, UserService } from '@universis/common';

@Component({
  selector: 'app-instructor-events',
  templateUrl: './instructor-event.component.html'
})
export class InstructorEventComponent implements OnInit {
  public upcomingEventsQuery: ClientDataQueryable;
  public events: any [] = [];
  public isLoading = true;

  constructor(
    private _context: AngularDataContext,
    private _loadingService: LoadingService,
    private _errorService: ErrorService,
    private _userService: UserService) {
    this.upcomingEventsQuery = this._context.model('instructors/me/TeachingEvents')
      .where('eventHoursSpecification').equal(null)
      .and('startDate').greaterOrEqual((new Date()).toISOString())
      .expand('location')
      .take(-1);
  }

  async ngOnInit() {
    this._loadingService.showLoading();
    try {
      let teachingEvents = await this._context.model('instructors/me/TeachingEvents')
        .select('TeachingEventSummary')
        .expand('sections', 'eventStatus', 'performer', 'location', 'courseClass($expand=course)', 'eventHoursSpecification')
        .orderBy('startDate')
        .take(-1)
        .getItems();

      if (teachingEvents) {
        teachingEvents.forEach(event => {
          if (!event.url && event.courseClass.course.id && event.courseClass.year && event.courseClass.period && event.id && event.performer.id && event.performer.id === this._userService.getUserSync().id) {
            event['url'] =  `/courses/${event.courseClass.course.id}/${event.courseClass.year}/${event.courseClass.period}/teachingEvents/${event.id}/attendance`;
          }
        });
      }

      let courseExamEvents = await this._context
        .model('instructors/me/CourseExamEvents')
        .select('CourseExamEventSummary')
        .expand('performer', 'location', 'courseExam', 'courseExam($expand=course($select=id,displayCode))')
        .orderBy('startDate')
        .take(-1)
        .getItems();

      this.events = teachingEvents.concat(courseExamEvents);

      this.isLoading = false;
      this._loadingService.hideLoading();
    } catch (error) {
      this.isLoading = false;
      this._loadingService.hideLoading();
      this._errorService.navigateToError(error);
    }
  }
}
