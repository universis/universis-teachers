import { NgModule } from '@angular/core';
import { environment } from '../../environments/environment';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { SharedModule } from '@universis/common';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CoursesSharedService } from './services/courses-shared.service';
import { CoursesService } from './services/courses.service';
import { EventAbsencesResolver } from "./event-configs.resolver";

// LOCALES: import extra locales here
import * as el from './i18n/courses.el.json';
import * as en from './i18n/courses.en.json';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        TranslateModule,
        ModalModule.forRoot()
    ],
    providers: [
        CoursesSharedService,
        CoursesService,
    ]
})
export class CoursesSharedModule {
    constructor(private _translateService: TranslateService) {
        this._translateService.setTranslation("el", el, true);
        this._translateService.setTranslation("en", en, true);
        // environment.languages.forEach((culture) => {
        //     import(`./i18n/courses.${culture}.json`).then((translations) => {
        //         this._translateService.setTranslation(culture, translations, true);
        //     });
        // });
    }
}

