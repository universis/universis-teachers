/**
 *
 * CoursesGradesModalComponent
 * It is the modal and handles the logic for the grades submission and the
 * grades digital signing.
 *
 * The digital signing is optional and it is configured at the institute
 * configuration.
 *
 *
 * A file upload consists of the following actions:
 *   - Upload the file (import) and get a file hash from the server
 *   - [optionally] sign the file hash
 *   - Verify the upload
 *
 * Modal is divided in three phases (steps). We refer to them by their index.
 * The index is based on zero(0) and the are the following:
 *   0: The user selects the file.
 *   1: The file is being uploaded, it's format and contents are verified and
 *      optionally signed
 *   2: The grade submission is verified by the user.
 *
 */

import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import 'smartwizard';
import { FormBuilder } from '@angular/forms';
import { CoursesService } from '../../services/courses.service';
import { TranslateService } from '@ngx-translate/core';
import { LoadingService, ConfigurationService } from '@universis/common';
import { ErrorService } from '@universis/common';
import { Subject, throwError } from 'rxjs';
import { ProfileService } from '../../../profile/services/profile.service';
import WebSocketAsPromised from 'websocket-as-promised';
import { ApplicationSettings } from 'src/app/teachers-shared/teachers-shared.module';
import moment from 'moment';

declare var $: any;

export class Counter {
  constructor(
    public code = '',
    public counter = 0,
    public message = ''
  ) {
  }
}


@Component({
  selector: 'app-courses-grades-modal',
  templateUrl: './courses-grades-modal.component.html'
})

export class CoursesGradesModalComponent implements OnInit {

  public courseExamId: number | any;      // current courseExam ID
  public resultsOfUpload;           // results of uploading file
  public resultsOfSubmission;       // results of complete action
  public courseExam;                // current courseExam object
  public waitForResults = false;    // This flag help go to next step if validate is success
  public errorMessage = '';
  public websocketErrorMessage = '';
  public isLoading = false;
  objectKeys = Object.keys;
  //
  slots_info: any = {};
  certs_info: any = {};
  selected_slot = '';
  selected_cert = '';
  slotSelectedCertInfo: any = {};
  selected_source_encoding = 'base64';
  selected_destination_encoding = 'base64';
  output = '...';
  // private ws: WebSocketAsPromised;
  signingResult = '';
  instructorEmail = '';
  checkTokenInfo: any | boolean = false;
  wsURI: any | string = '';
  //
  public message = '';
  public haveChangesGrades = false;
  public haveSignedDocument = false;
  public counters = [];                 // counters with sums for every grade result state
  public submitGradesCounters = {
    success: 0,
    unSuccess: 0
  };
  public uploadGradesCounters = {
    success: 0,
    unSuccess: 0
  };
  public useDigitalSignature = false;   // institute configuration setting
  // Native app variables
  public socket_open = false;
  error = '';
  public ws?: WebSocketAsPromised;
  public pluggedDevice = false;

  @ViewChild('wizard') wizard: ElementRef | any;
  public selectedFile: File | any;        // current selected file for upload

  public result: Subject<boolean> = new Subject<boolean>(); // result for modal closing
  public resultSuccessStatus = false;      // true : reload data
  public errorCount: number = 0;

  constructor(private modalRef: BsModalRef,
    private fb: FormBuilder,
    private courseServices: CoursesService,
    private translate: TranslateService,
    private loadingService: LoadingService,
    private errorService: ErrorService,
    private profile: ProfileService,
    private _configurationService: ConfigurationService) {
    const appSetting = this._configurationService.settings.app;
    this.checkTokenInfo = appSetting && (<ApplicationSettings>appSetting).checkTokenInfo;
    this.wsURI = appSetting && (<ApplicationSettings>appSetting).websocket;
  }

  async ngOnInit() {
    const self = this;
    $(this.wizard.nativeElement).smartWizard({
      anchorSettings: {
        anchorClickable: true,           // Enable/Disable anchor navigation
        enableAllAnchors: false,         // Activates all anchors clickable all times
        markDoneStep: true,              // add done css
        enableAnchorOnDoneStep: true,    // Enable/Disable the done steps navigation
      },
      useURLhash: false,
      showStepURLhash: false,
      lang: {
        next: this.translate.instant('CoursesLocal.Modal.Step1.Next'),
        previous: this.translate.instant('CoursesLocal.Modal.Buttons.Prev')
      },
      toolbarSettings: {
        toolbarExtraButtons: [
          $('<button></button>').text(this.translate.instant('CoursesLocal.Modal.Buttons.Cancel'))
            .addClass('btn btn-secondary btn-close')
            .on('click', async function () {
              if (self.useDigitalSignature) {
                try {
                  const response = await self.sendRequest({
                    command: 'close_session',
                    slot_serial: self.selected_slot
                  });
                  self.slots_info[self.selected_slot].open = false;
                  await self.ws?.close();
                } catch (error) {
                  console.error(error);
                }
              }
              self.closeModal();
            })
        ]
      },
    });
    self.hideButtonPrevious(true);
    self.disabledButtonNext(true);

    // call next function and validate data
    $(this.wizard.nativeElement).on('leaveStep', async (e, anchorObject, stepNumber, stepDirection) => {

      if (!self.selectedFile) {
        self.errorMessage = self.translate.instant('CoursesLocal.Modal.Step1.NoFoundFile');
        return false;
      }

      if (stepDirection === 'forward' && stepNumber === 0) {
        // when you leave step 1 and go to step 2 - The user uploads the selected file
        if (!self.waitForResults) {
          self.loadingService.showLoading();
          this.isLoading = true;
          await self.uploadGradesFile();
          return false;
        }
      } else if (stepDirection === 'forward' && stepNumber === 1) {
        // when you leave step 1 and go to step 2 - The user uploads the file
        if (self.haveChangesGrades) {/*&& !self.waitForResults*/
          self.loadingService.showLoading();
          this.isLoading = true;
          try {
            await self.setUploadToComplete();
          } catch (err) {
            console.error('upload error: ', err);
          } finally {
            this.loadingService.hideLoading();
            this.isLoading = false;
          }

          return false;
        } else if (!self.haveChangesGrades) {
          return false;
        }
      } else if (stepDirection === 'backward' && stepNumber === 1) {
        // when you leave step 2 and go to step 1
        if (!self.socket_open && self.useDigitalSignature) {
          self.displayWebSocketError();
          self.disabledButtonNext(true);
        }
        self.hideButtonPrevious(true);
      }
    });

    // runs at the loading of every step
    $(this.wizard.nativeElement).on('showStep', function (e, anchorObject, stepNumber, stepDirection) {

      // The user already selected and uploaded a file (but they did not
      // confirmed the submission) and they want to upload another file.
      // when you show step 1
      if (stepNumber === 0) {
        self.counters = [];
        self.submitGradesCounters = {
          success: 0,
          unSuccess: 0
        }
        self.uploadGradesCounters = {
          success: 0,
          unSuccess: 0
        }
        self.waitForResults = false;
        self.message = '';
        self.errorMessage = '';
        self.haveChangesGrades = false;
        self.hideButtonPrevious(true);
        self.changeTextOnButtonNext(self.translate.instant('CoursesLocal.Modal.Step1.Next'));
      }

      if (stepNumber === 1) {
        // when you show step 2 - The user has submitted a file for upload
        self.waitForResults = false;
        if (self.useDigitalSignature) {
          self.changeTextOnButtonNext(self.translate.instant('CoursesLocal.Modal.Step2.NextAndSign'));
        } else {
          self.changeTextOnButtonNext(self.translate.instant('CoursesLocal.Modal.Step2.NextAndSubmit'));
        }
        self.hideButtonNext(false);
        self.hideButtonPrevious(false);

        self.disabledButtonNext(true);

      } else if (stepNumber === 2 && stepDirection === 'forward') {
        // when you show step 2 and come from step 1 - The user verified the grades submission.
        $(self.wizard.nativeElement).smartWizard('next');
        self.hideButtonNext(true);
        self.changeTextOnButtonClose(self.translate.instant('CoursesLocal.Modal.Buttons.Close'));
      }
    });

    // This function is blocking (because of the await) and should be called
    // after the UI initialization.
    await this.bootstrap();
  }

  /**
   *
   * Fetches essential data for the grades submission modal.
   *
   */
  async bootstrap() {
    // OPTIMIZE: Consider using Promise.all when bootstrap the component
    // (for instructor, courseExam and optionally the socket opening socket);

    // get the instructor and the department configuration ( needed for digital signature)
    try {
      const instructor = await this.profile.getInstructor();
      this.instructorEmail = instructor.email;
      this.useDigitalSignature = instructor.department.organization.instituteConfiguration.useDigitalSignature;
    } catch (err) {
      return this.errorService.navigateToError(err);
    }

    // Bootstrap the web socket.
    if (this.useDigitalSignature) {
      this.ws = new WebSocketAsPromised(this.wsURI, {
        packMessage: data => JSON.stringify(data),
        unpackMessage: (data: string | any) => JSON.parse(data),
        attachRequestId: (data, requestId) => Object.assign({ request_id: requestId }, data),
        extractRequestId: data => data && data.request_id
      });
      this.ws.onClose.addListener(() => {
        this.socket_open = false;
        this.displayWebSocketError();
      });

      try {
        await this.openSocket();
      } catch (err) {
        console.error('socket err: ', err);
        return this.errorService.navigateToError(err);
      }
    }

    await this.getCourseExam();
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  displayWebSocketError() {
    if (this._configurationService.settings.app && (<ApplicationSettings>this._configurationService.settings.app).websocket) {
      this.websocketErrorMessage = this.translate.instant('CoursesLocal.Modal.Step2.WebsocketNotOpen');
    } else {
      this.websocketErrorMessage = this.translate.instant('CoursesLocal.Modal.Step2.WebSocketNotSetInApp');
    }
  }

  openViaChromeExtension() {
    window.postMessage({ type: 'UNIVERSIS', text: 'Native app is running' }, '*');
  }

  async openSocket() {
    try {
      this.loadingService.showLoading();
      // window.postMessage({ type: 'UNIVERSIS', text: 'Native app is running' }, '*');
      await this.ws?.open();
      this.socket_open = true;
      this.listSlots();
      this.websocketErrorMessage = '';
      this.loadingService.hideLoading();
    } catch (error) {
      this.error = error;
      console.error(error);
      if (this.useDigitalSignature) {
        this.displayWebSocketError();
        if (error && this.errorCount < 3) {
          ++this.errorCount;
          try {
            await this.openViaChromeExtension();
            await new Promise(resolve => setTimeout(resolve, 3000)); // sleeps for 3 seconds while app is launching
            return await this.openSocket();
          } catch (err) {
            console.error(err);
            return;
          }
        }
        this.loadingService.hideLoading();
      }
    }
  }

  async listSlots() {
    try {
      this.selected_cert = '';
      const response = await this.sendRequest({
        command: 'list_slots'
      });
      this.slots_info = response.payload;
      //
      if (Object.keys(this.slots_info).length > 0) {
        this.pluggedDevice = true;
      } else {
        this.errorMessage = this.translate.instant('CoursesLocal.Modal.Step2.TokenNotPresentMessage');
      }
      //
      if (Object.keys(this.slots_info).length === 1) {
        this.selected_slot = Object.keys(this.slots_info)[0];
        this.openSession();
      }
    } catch (error) {
      console.error(error);
    }
  }

  async openSession() {
    try {
      const response = await this.sendRequest({
        command: 'open_session',
        slot_serial: this.selected_slot
      });
      this.slots_info[this.selected_slot].open = true;
      this.loginSession();
      this.listCerts();
    } catch (error) {
      if (error.message === this.translate.instant('CoursesLocal.Modal.Step2.TokenNotPresentCode')) {
        this.errorMessage = this.translate.instant('CoursesLocal.Modal.Step2.TokenNotPresentMessage');
      }
      console.error(error);
    }
  }

  async loginSession() {
    try {
      const response = await this.sendRequest({
        command: 'login_session',
        slot_serial: this.selected_slot
      });
      this.slots_info[this.selected_slot].logged = true;
    } catch (error) {
      console.error(error);
    }
  }

  async sendRequest(payload: any) {
    if (this.socket_open) {
      const response = await this.ws?.sendRequest(payload);
      if (response.error) {
        throw new Error(response.error);
      }
      return response;
    }
    throw Error('Websocket not open.');
  }

  selectCert() {
    this.openSession();
    this.loginSession();
    this.listCerts();
    this.errorMessage = '';
    // Disable sign button
    this.disabledButtonNext(true);
    this.selected_cert = '';
  }

  async listCerts() {
    this.errorMessage = '';
    try {
      const { payload: currentSlotCerts } = await this.sendRequest({
        command: 'list_certs',
        slot_serial: this.selected_slot
      });
      const parsedCertificates = new Map<string, any>();
      Object.entries(currentSlotCerts).forEach(([certKey, certificate]) => {
        try {
          let currentCert = this.extractCertificateInfo(certKey, certificate);
          if (this.checkTokenInfo && currentCert) {
            currentCert = this.validateCertificate(currentCert);
          }
          if (currentCert) {
            parsedCertificates.set(certKey, currentCert);
          }
        } catch (err) {
          console.warn(err);
        }
      });

      // Transform map to array in order to keep the sorting at  ngTemplate
      const certsArray: any[] = [];
      parsedCertificates.forEach((cert, key) => {
        certsArray.push({
          key,
          value: cert
        });
      });

      // The certificates the user can see
      this.certs_info = certsArray;

      // Store a second instance of the certificates in a list with only the valid certs as received from
      // the signing app. The user can see them all, but can sign only with the valid certs
      if (this.checkTokenInfo) {
        const validatedCertificates = {};
        parsedCertificates.forEach((cert, key) => {
          if (!(<Map<any, any>>cert).get('errors')) {
            validatedCertificates[key] = currentSlotCerts[key];
          }
        });
        this.slots_info[this.selected_slot].certificates = validatedCertificates;
      } else {
        this.slots_info[this.selected_slot].certificates = currentSlotCerts;
      }

      const validatedCertsArray = Object.keys(this.slots_info[this.selected_slot].certificates);
      if (currentSlotCerts.length === 0) {
        this.errorMessage = this.translate.instant('CoursesLocal.Modal.Step2.NotAvailableCertificates');
      } else if (validatedCertsArray.length === 0) {
        this.errorMessage = this.translate.instant('CoursesLocal.Modal.Step2.NoValidCertificate');
      } else if (validatedCertsArray.length === 1) {
        this.selected_cert = validatedCertsArray[0];
      }
    } catch (error) {
      if (error.message === 'Websocket not open.') {
        this.errorMessage = this.translate.instant('CoursesLocal.Modal.Step2.WebsocketNotOpen');
      }
      console.error(error);
    }
  }

  /**
   * ===============================================================
   * Getters
   * ===============================================================
   */

  get selectedSlotOpen() {
    return this.selected_slot !== '' && this.slots_info[this.selected_slot].open;
  }

  get selectedSlotLogged() {
    return this.selected_slot !== '' && this.slots_info[this.selected_slot].logged;
  }

  get selectedCert() {
    return this.selected_cert !== '';
  }

  get signFlag() {
    return this.socket_open
      && this.selectedSlotOpen
      && this.selectedSlotLogged
      && this.selectedCert;
  }

  get slotSelectedSlotInfo() {
    if (this.selected_slot) {
      return this.slots_info[this.selected_slot];
    }
    return false;
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  /**
   *
   * Verifies the instructor's signature
   *
   * Sends the signed (by the instructor) hash to the API
   *
   */
  async gradeSigning(additionalResult) {

    try {
      const signingResult = await this.courseServices.signCourseExamDocument(
        this.courseExamId,
        this.resultsOfUpload.id,
        additionalResult
      );

      const signedDocument = signingResult.additionalResult.signedDocument;
      const signatureBlock = signingResult.additionalResult.signatureBlock;
      const userCertificate = signingResult.additionalResult.userCertificate;
      const checkHashKey = signingResult.additionalResult.checkHashKey;

      if (signedDocument && signatureBlock && userCertificate && checkHashKey) {
        $(this.wizard.nativeElement).smartWizard('next'); // go to final step
        this.resultsOfSubmission.object.validationResult.success = true;
        this.haveSignedDocument = true;
      } else {
        this.resultsOfSubmission.object.validationResult.success = false;
      }
    } catch (err) {
      console.error('signing attempt error: ', err);
      this.errorMessage = `"First attempt" ${err}`;
      throw new Error('Error while signing the app;');
    }
  }

  async cancelCourseExamDocument() {
    try {
      const cancelRes = await this.courseServices.cancelCourseExamDocument(this.courseExamId, this.resultsOfUpload.id);
      // If cancelled successfully
      if (cancelRes.actionStatus && cancelRes.actionStatus.alternateName === 'CancelledActionStatus') {
        this.errorMessage = this.translate.instant('CoursesLocal.Modal.Step2.SigningFailed');
        this.message = this.translate.instant('CoursesLocal.Modal.Step2.CancelledSuccessfullyAtSigning');
      } else {
        this.message = '';
        const signingFailed = this.translate.instant('CoursesLocal.Modal.Step2.SigningFailed');
        const cancelFailedAtSigning = this.translate.instant('CoursesLocal.Modal.Step2.CancelFailedAtSigning');
        this.errorMessage = signingFailed + ' ' + cancelFailedAtSigning;
      }

    } catch (err) {
      console.warn('The upload could not be canceled', err);
    }
  }

  // second step. Triggers the file "import"
  async uploadGradesFile() {
    if (this.selectedFile) {

      try {
        const uploadedFile = await this
          .courseServices
          .uploadGradesFile(this.selectedFile, this.courseExamId)
          .toPromise();
        this.resultsOfUpload = uploadedFile;

        if (this.resultsOfUpload.object.validationResult.success) {
          this.errorMessage = '';
          this.message = this.resultsOfUpload.object.validationResult.message;

          let changeExist = false;

          // group students by validation result code
          let counters: any[] = [];
          for (const grade of this.resultsOfUpload.object.grades) {
            if (grade.validationResult.code !== 'UNMOD') {
              changeExist = true;
              this.haveChangesGrades = true;
              let index = -1;
              if (counters.length > 0) {
                index = counters.findIndex((c: any) => c.code === grade.validationResult.code);
              }

              if (index >= 0) {
                counters[index].counter++;
              } else {
                counters.push(new Counter(
                  grade.validationResult.code,
                  1,
                  grade.validationResult.message
                ));
              }
            }
          }

          this.waitForResults = false;
          this.uploadGradesCounters.success = 0;
          counters.forEach(x => {
            if (x.code === 'INS' || x.code === 'UPD' || x.code === 'SUCC') {
              this.uploadGradesCounters.success += x.counter;
            }
          }
          );
          this.uploadGradesCounters.unSuccess = 0;
          counters.forEach(x => {
            if (x.code === 'EAVAIL' || x.code === 'EFAIL' ||
              x.code === 'ERANGE' || x.code === 'ESTATUS') {
              this.uploadGradesCounters.unSuccess += x.counter;
            }
          }
          );

          // transform partial success to error.
          if (this.resultsOfUpload.object.validationResult.success &&
            this.resultsOfUpload.object.grades.length === 0) {
            changeExist = true;
            this.haveChangesGrades = true;
          }

          if (this.resultsOfUpload.object.validationResult.code === 'PSUCC') {
            this.errorMessage = this.translate.instant('CoursesLocal.Modal.Step2.PartialSuccess');
          }

          this.disabledButtonNext(!changeExist);
          this.loadingService.hideLoading();
          this.isLoading = false;

        } else {
          this.errorMessage = this.translate.instant('CoursesLocal.Modal.Step2.NotValidFile');
          this.loadingService.hideLoading();
          this.isLoading = false;
        }
      } catch (err) {
        console.warn('error uploading the file: ', err);
        // when statusCode of validationResult exist get this
        const statusCode = err.error
          && err.error.object
          && err.error.object.validationResult
          ? err.error.object.validationResult.statusCode
          : 0;
        if (statusCode !== 0) {
          // get error message key
          const errorMessageKey = `CoursesLocal.Modal.Errors.E${statusCode || err.status || 500}.message`;
          // try to get localized message
          const errorMessage = this.translate.instant(errorMessageKey);
          // if custom error does not exist errorMessage should be equal to errorMessageKey
          if (errorMessage === errorMessageKey) {
            // get error message from common http errors
            this.errorMessage = this.translate.instant(`E${statusCode || err.status}.message`);
          } else {
            // set custom http error message
            this.errorMessage = errorMessage;
          }
        } else {
          this.errorMessage = err.message;
        }

        this.loadingService.hideLoading();
        this.isLoading = false;
      }
    }
  }

  // final step
  async setUploadToComplete() {
    if (this.selectedFile) {
      let setUploadResponse;

      // Complete the upload action.
      try {
        //throw Error('There is no selected certificate.');

        setUploadResponse = await this
          .courseServices
          .setUploadToComplete(this.courseExamId, this.resultsOfUpload.id);
        this.resultsOfSubmission = setUploadResponse;
        this.message = this.resultsOfSubmission.object.validationResult.message;
        // group students by validation result code
        let counters: any[] = [];
        for (const grade of this.resultsOfSubmission.object.grades) {
          if (grade.validationResult.code !== 'NOACTION') {

            this.haveChangesGrades = true;
            let index = -1;
            if (counters.length > 0) {
              index = counters.findIndex(c => c.code === grade.validationResult.code);
            }

            if (index >= 0) {
              counters[index].counter++;
            } else {
              counters.push(new Counter(
                grade.validationResult.code,
                1,
                grade.validationResult.message
              ));
            }
          }
        }

        this.submitGradesCounters.success = 0;
        counters.forEach(x => {
          if (x.code === 'INS' || x.code === 'UPD' || x.code === 'SUCC') {
            this.submitGradesCounters.success += x.counter;
          }
        }
        );
        this.submitGradesCounters.unSuccess = 0;
        counters.forEach(x => {
          if (x.code === 'EAVAIL' || x.code === 'EFAIL' ||
            x.code === 'ERANGE' || x.code === 'ESTATUS' || x.code === 'INVDATA') {
            this.submitGradesCounters.unSuccess += x.counter;
          }
        }
        );

        /* // transform partial success to error.
         if (this.submitGradesCounters.success === 0) {
           this.errorMessage = this.translate.instant('CoursesLocal.Modal.Step2.GradesUpdateDescription.none');
           throw new Error(this.errorMessage);
         }*/

        if (this.resultsOfSubmission.object.validationResult.code === 'PSUCC') {
          this.errorMessage = this.translate.instant('CoursesLocal.Modal.Step3.PartialSuccess');
        }

      } catch (err) {
        console.warn('complete err: ', err);
        if (this.useDigitalSignature) {
          await this.cancelCourseExamDocument();
        } else {
          // get error message key
          const errorMessageKey = `CoursesLocal.Modal.Errors.E${err.status}.message`;
          this.waitForResults = true;
          $(this.wizard.nativeElement).smartWizard('next'); // go to third step
          // try to get localized message
          const errorMessage = this.translate.instant(errorMessageKey);
          // if custom error does not exist errorMessage should be equal to errorMessageKey
          if (errorMessage === errorMessageKey) {
            // get error message from common http errors
            if (err.status) {
              this.errorMessage = this.translate.instant(`E${err.status}.message`);
            } else {
              this.errorMessage = err.message;
            }
          } else {
            // set custom http error message
            this.errorMessage = errorMessage;
          }
        }
      }

      if (this.resultsOfSubmission.object.validationResult.success) {
        if (this.resultsOfSubmission.object.validationResult.code === 'PSUCC') {
          this.errorMessage = this.translate.instant('CoursesLocal.Modal.Step3.PartialSuccess');
        }
        this.hideButtonPrevious(true);
      } else {
        this.errorMessage = this.translate.instant('CoursesLocal.Modal.Step3.NotSuccessSubmit');
        this.hideButtonPrevious(false);
      }

      this.resultSuccessStatus = true;
      this.waitForResults = true;

      if (this.useDigitalSignature) {
        // Add grade signing here
        try {
          await this.createSigningResponse();
          $(this.wizard.nativeElement).smartWizard('next'); // go to third step
          this.loadingService.hideLoading();
          this.isLoading = false;
        } catch (err) {
          console.error('err: ', err);
          this.errorMessage = 'Create signing response failed.';
          // TODO: report this
        }
      }
    }
  }


  /**
   *
   * Given a payload (the hash of the file), the token signs the hash with the
   * instructor certificate and returns that signed hash.
   *
   */
  async createSigningResponse() {
    if (this.selected_cert.length > 0) {
      if (this.checkTokenInfo) {
        const currentCertData = (this.certs_info.find(({ key }) => key === this.selected_cert));
        const validated = this.validateCertificate(currentCertData.value);
        if (validated.get('errors')) {
          throw new Error(`Certificate ${this.selectedCert} is not validated`);
        }
      }

      try {
        const response = await this.sendRequest({
          command: 'sign_with_cert',
          slot_serial: this.selected_slot,
          cert_id: this.selected_cert,
          message: this.resultsOfSubmission.additionalResult.checkHashKey,
          source_encoding: this.selected_source_encoding,
          destination_encoding: this.selected_destination_encoding
        });

        this.output = response.payload.signature;
        // Get user certificate from selected cert key

        if (
          this.slotSelectedSlotInfo
          && Object.keys(this.slotSelectedSlotInfo['certificates']).length > 0
          && this.selected_cert.length > 0) {

          const selectedCert = this.slotSelectedSlotInfo.certificates[this.selected_cert];

          if (selectedCert) {
            this.slotSelectedCertInfo = selectedCert['info'];
            this.resultsOfSubmission.additionalResult.userCertificate = selectedCert['raw'];
            this.resultsOfSubmission.additionalResult.signatureBlock = this.output;
            this.errorMessage = '';
            await this.gradeSigning(this.resultsOfSubmission.additionalResult);
          } else {
            throw Error('There is no selected certificate.');
          }
        }
      } catch (error) {
        // TODO: log the error
        console.warn('Failed to sign the grades file.', error);
        try {
          this.resultsOfSubmission.object.validationResult.success = false;
          await this.cancelCourseExamDocument();
        } catch (err) {
          console.warn('Cancel action failed.');
        }
      }
    } else {
      this.resultsOfSubmission.additionalResult.userCertificate = null;
      this.resultsOfSubmission.additionalResult.signatureBlock = null;
      this.errorMessage = this.translate.instant('CoursesLocal.Modal.Step2.CertificateNotValid');
      // Disable sign button
      const btnnext = document.getElementsByClassName('sw-btn-next')[0];
      btnnext.setAttribute('disabled', 'true');
    }
  }

  async getCourseExam() {
    try {
      const courseExam = await this.courseServices.getCurrentCourseExams(this.courseExamId);
      this.courseExam = courseExam;
      if (this.courseExam.status.alternateName === 'closed') {
        this.disabledButtonNext(true);
        this.errorMessage += this.translate.instant('CoursesLocal.Modal.Step1.ClosedExam');
      }
    } catch (err) {
      this.errorService.navigateToError(err);
    }
  }

  getInputFileName(newFile) {
    this.selectedFile = newFile;
    this.disabledButtonNext(false);
    this.errorMessage = '';
  }

  closeModal() {
    // If teacher should go through digital signing and has already submitted the xls (originalDocument has been created)
    if (this.useDigitalSignature && this.resultSuccessStatus && !this.haveSignedDocument) {
      this.cancelCourseExamDocument().then(res => {
        this.result.next(this.resultSuccessStatus);
        this.modalRef.hide();
      }, (cancelErrr) => {
        console.error(cancelErrr);
      });
    } else {
      this.result.next(this.resultSuccessStatus);
      this.modalRef.hide();
    }
  }

  disabledButtonNext(status) {
    const btnnext = document.getElementsByClassName('sw-btn-next')[0];
    if (status) {
      btnnext.setAttribute('disabled', 'true');
    } else {
      btnnext.removeAttribute('disabled');
    }
  }

  disabledButtonPrev(status) {
    const btnprev = document.getElementsByClassName('sw-btn-prev')[0];
    if (status) {
      btnprev.setAttribute('disabled', 'true');
    } else {
      btnprev.removeAttribute('disabled');
    }
  }

  hideButtonNext(status) {
    const btnnext = document.getElementsByClassName('sw-btn-next')[0];
    if (status) {
      btnnext.setAttribute('style', 'display:none');
    } else {
      btnnext.setAttribute('style', 'display:block');
    }
  }

  hideButtonPrevious(status) {
    const btnprev = document.getElementsByClassName('sw-btn-prev')[0];
    if (status) {
      btnprev.setAttribute('style', 'display:none');
    } else {
      btnprev.setAttribute('style', 'display:block');
    }
  }

  changeTextOnButtonNext(text) {
    const btnnext = document.getElementsByClassName('sw-btn-next')[0];
    btnnext.innerHTML = text;
  }

  changeTextOnButtonClose(text) {
    const btnnext = document.getElementsByClassName('btn btn-secondary btn-close')[0];
    btnnext.innerHTML = text;
  }

  /**
   *
   * Extracts the attributes of an x.509 certificate
   *
   * @param {string} certKey The key that the native app assigns to the certificate
   * @param {object} certificate The certificate's attributes
   *
   */
  extractCertificateInfo(certKey: string, certificate: any): Map<string, any> {
    if (certKey.length < 0) {
      throw new Error(`Empty key received`);
    }

    const subject = certificate['info']['subject'].split(',').map(pair => pair.split('='));
    const attributes = new Map<string, any>(subject);
    const ou = <string>attributes.get('OU'); // Personal certificate should contain OU property

    if (!ou) {
      throw new Error(`OU was not found for ${certKey}`);
    }

    attributes.set('OU', ou.substring(0, ou.lastIndexOf('-') - 1)); // Get only Class A, Class B info
    attributes.set('fromDate', moment(certificate['info']['not_valid_before']).format('YYYY-MM-DD')); // Set from date
    attributes.set('toDate', moment(certificate['info']['not_valid_after']).format('YYYY-MM-DD')); // Set to date

    return attributes;
  }

  /**
   *
   * Validates a certificate against it's expiration date and the instructor email
   *
   * @param certificate The certificate to be validated
   *
   */
  validateCertificate(certificate: Map<string, any>, testEmail = true, testDates = true): Map<string, any> {
    const validFrom = new Date(certificate.get('fromDate')).getTime();
    const validTo = new Date(certificate.get('toDate')).getTime();
    const email = certificate.get('1.2.840.113549.1.9.1'); // Email property
    const now = new Date().getTime();
    const errors: any[] = [];

    // NOTE: errors must begin with a capital letter and end with a period.
    if (email !== this.instructorEmail && testEmail) {
      errors.push(this.translate.instant('CoursesLocal.Modal.Step2.CertificateEmailNotValid'));
    }

    if (validFrom > now || validTo < now && testDates) {
      errors.push(this.translate.instant('CoursesLocal.Modal.Step2.CertificateExpired'));
    }

    if (errors.length > 0) {
      certificate.set('errors', errors);
    }

    return certificate;
  }
}
