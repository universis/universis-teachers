import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-courses-grades-step-3',
  templateUrl: './courses-grades-step-3.component.html',
  styles: [`
      .list-unsuccess-grades {
        max-height: 120px;
        overflow-y: scroll;
        overflow-x: hidden;
      }
    `]
})
export class CoursesGradesStep3Component implements OnInit {

  @Input() resultsOfSubmission;            // data with results
  @Input() useDigitalSignature;
  @Input() errorMessage = '';
  @Input() message = '';
  @Input() courseExam;      // current courseExam object
  @Input() counters;          // counters with sums for every grade result state
  @Input() isLoading = false;
  @Input() resultsOfUpload;   // data with results

  gradesUpdateMapping: any = {
    '=0': 'CoursesLocal.Modal.Step3.GradesUpdatedDescription.none',
    '=1': 'CoursesLocal.Modal.Step3.GradesUpdatedDescription.singular',
    'other': 'CoursesLocal.Modal.Step3.GradesUpdatedDescription.plural'
  };

  gradesNotUpdateMapping: any = {
    '=0': 'CoursesLocal.Modal.Step3.GradesNotUpdatedDescription.none',
    '=1': 'CoursesLocal.Modal.Step3.GradesNotUpdatedDescription.singular',
    'other': 'CoursesLocal.Modal.Step3.GradesNotUpdatedDescription.plural'
  };

  constructor() { }

  ngOnInit() {

  }
}
