import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-courses-details-tabs',
  templateUrl: './courses-details-tabs.component.html',
  styleUrls: ['./courses-details-tabs.component.scss']
})
export class CoursesDetailsTabsComponent implements OnInit {

  @Input() count?: string;

  constructor() { }

  ngOnInit() {
  }

}
