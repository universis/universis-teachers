import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ToastService } from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';
import { assign } from 'lodash';
import * as FORM_CONFIG from './course-elearning-division.json';
import { ServiceUrlPreProcessor, TranslatePreProcessor } from '@universis/forms';
import { FormioComponent, FormioRefreshValue } from '@formio/angular';

interface DivisionItem {
  id?: number,
  title?: string,
  classIndex?: number,
  instructors?: [
    {
      id?: number,
      familyName?: string,
      givenName?: string
    }
  ]
}

@Component({
  selector: 'app-courses-elearning-division',
  template: `
  <div class="mx-3" [translate]="'CourseDivisionModal.Description'"></div>
  <div class="container my-3">
    <ng-container *ngFor="let item of items; let i = index">
      <formio
        #formio
        (change)="onChange($event, item)"
        [submission]="{ data: item }"
        [form]="form"
        [refresh]="refreshForm"
        [renderOptions]="renderOptions">
      </formio>
      <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
        <button type="button" (click)="remove(i)" class="btn btn-danger ml-auto" [disabled]="items.length === 1">
          {{ 'CoursesLocal.Delete' | translate }} (-)
        </button>
      </div>
    </ng-container>
    <div class="row mt-4">
      <button type="button" [disabled]="!isValid" (click)="add()" class="btn btn-block btn-lg btn-secondary mx-3">
        {{ 'CoursesLocal.Add' | translate }} (+)
      </button>
    </div>
  </div>`
})
export class CoursesElearningDivisionComponent extends RouterModalOkCancel implements OnInit, OnDestroy {
  @Output() refreshForm: EventEmitter<FormioRefreshValue> = new EventEmitter();
  @ViewChild('formio') formio: FormioComponent;
  public renderOptions?: {
    language: string;
    i18n: any;
  };
  public form: any;
  public items: DivisionItem[] = [{
    id: undefined,
    title: '',
    instructors: [{}]
  }]
  public refresh = new EventEmitter<any>();
  public currentClass: any;
  public currentClassTitle: string;
  public currentClassDisplayCode: string;
  public isValid: boolean = false;

  private removed: DivisionItem[] = [];

  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    private _context: AngularDataContext,
    private translate: TranslateService,
    private toastService: ToastService) {

    super(router, route);
    this.modalClass = 'modal-xl';
  }

  ngAfterViewInit(): void {
    this.route.data.subscribe(async data => {
      try {
        this.currentClass = data['courseClass'];
        this.currentClassTitle = this.currentClass.title;
        this.currentClassDisplayCode = this.currentClass.course.displayCode;
        const classDivisions = await this._context
          .model(`instructors/me/classes/${this.currentClass.id}/oneroster/classes`)
          .select('id, title, instructors, classIndex')
          .expand('instructors')
          .where('status')
          .equal('active')
          .getItems();

        if (classDivisions.length > 0) {
          const divisionInstructors = await this._context
            .model(`Instructors/me/classes/${this.currentClass.id}/instructors`)
            .select('id,instructor')
            .expand('instructor($select=InstructorSummary)')
            .getItems();
          // for the title, remove the class title prefix and only take the suffix that is inside the parentheses
          this.items = classDivisions.map((division: DivisionItem) => ({
            id: division.id,
            title: division.title,
            classIndex: division.classIndex,
            instructors: divisionInstructors.filter((first: any) => division.instructors.some(second => second.id === first.instructor.id)).map((instructor: any) => instructor.instructor)
          }));
        }
        let form = Object.assign(FORM_CONFIG, { currentClass: this.currentClass })
        form = new ServiceUrlPreProcessor(this._context).parse(form);
        this.form = new TranslatePreProcessor(this.translate).parse(form);
        this.initializeRenderOptions();
        this.modalTitle = this.translate.instant('CourseDivisionModal.Title', { displayCode: this.currentClassDisplayCode, class: this.currentClassTitle });
      } catch (err) {
        console.log(err);
      }
    })
  }

  ngOnDestroy(): void { }
  ngOnInit(): void {
  }
  add() {
    this.items.push({});
    this.isValid = false;
    this.okButtonDisabled = true;
  }

  remove(index: number) {
    const remove: DivisionItem[] = this.items.splice(index, 1);
    if (remove[0].id) {
      this.removed.push(remove[0]);
    }
    this.isValid = this.formio.formio.checkValidity();
  }

  onChange(event: { changed?: any; data?: any; isValid: boolean }, item: any) {
    if (event.changed) {
      this.isValid = event.isValid;
      this.okButtonDisabled = this.isValid ? false : true;
      assign(item, event.data)
    }
  }

  cancel(): Promise<any> {
    return this.close({ skipLocationChange: true });
  }

  initializeRenderOptions() {
    const currentLang = this.translate.currentLang;
    const translation = this.translate.instant('Forms');
    if (this.form && this.form.settings && this.form.settings.i18n) {
      // try to get local translations
      if (Object.prototype.hasOwnProperty.call(this.form.settings.i18n, currentLang)) {
        // assign translations
        Object.assign(translation, this.form.settings.i18n[currentLang]);
      }
    }
    // prepare i18n object
    const i18n = Object.defineProperty({}, currentLang, {
      configurable: true,
      enumerable: true,
      writable: true,
      value: translation
    });
    // and initialize renderOptions
    this.renderOptions = {
      language: currentLang,
      i18n
    };
  }

  async ok(): Promise<any> {
    try {
      // get the current divisions
      const classDivisions: DivisionItem[] = await this._context
        .model(`instructors/me/classes/${this.currentClass.id}/oneroster/classes`)
        .select('id, title, instructors, status, classIndex')
        .expand('instructors')
        .getItems();

      classDivisions.forEach((division: any) => {
        const remove = this.removed.findIndex((x: DivisionItem) => x.id === division.id);
        // if the division got removed make the status inactive
        Object.assign(division, {
          status: remove >= 0 ? 'inactive' : division.status
        });

        // if the division did not get removed
        if (remove === -1 && division.status !== 'inactive') {
          // get the form item that corresponds to the division
          const formItem = this.items.filter(element => element.id === division.id)[0];
          // get the instructors from the form item
          const instructors = formItem.instructors;
          // push the division instructors that got removed with $state = 4
          for (const instructor of division.instructors) {
            if (!instructors.find(element => element.id === instructor.id)) {
              instructor.$state = 4;
              instructors.push(instructor);
            }
          }
          // assign the two form fields
          Object.assign(division, {
            title: formItem.title,
            instructors
          });
        }
      });

      let index = classDivisions.length;
      // find divisions that are not saved
      const insert = this.items.filter((item) => !item.id).map((item) => {
        index++;
        // set class index
        const instructors = item.instructors;
        return Object.assign(item, {
          instructors,
          classIndex: index
        });
      });
      // pust new items to existing items
      classDivisions.push(...insert);

      await this._context.model(`instructors/me/classes/${this.route.snapshot.data['courseClass'].id}/oneroster/classes`)
        .save(classDivisions);

      this.toastService.show(
        this.translate.instant('CourseDivisionModal.SuccessTitle'),
        this.translate.instant(('CourseDivisionModal.SuccessMessage'))
      );
      // close modal
      this.close({
        fragment: 'reload',
        skipLocationChange: true
      });
    } catch (err) {
      // show fail toast message
      console.log(err)
      this.toastService.show(
        this.translate.instant('CourseDivisionModal.FailTitle'),
        this.translate.instant(('CourseDivisionModal.FailMessage'))
      );
      // close modal without triggering a reload
      this.cancel();
    }
  }
}
