import { Component, OnInit, OnDestroy } from '@angular/core';
import { CoursesService } from '../../services/courses.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigurationService, DiagnosticsService, ErrorService, GradeScaleService } from '@universis/common';
import { LoadingService } from '@universis/common';
import { ProfileService } from '../../../profile/services/profile.service';
import { Subscription } from 'rxjs';
import { AngularDataContext } from '@themost/angular';

interface CourseMerge {
  id: string,
  replacedBy: {
    id: string,
    title: string,
    displayCode: string,
    yearName: string,
    periodName: string,
    departmentName: string
  },
  courseClass: {
    id: string,
    title: string,
    displayCode: string,
    yearName: string,
    periodName: string,
    departmentName: string
  }
}

interface CourseDivision {
  id: string,
  title: string,
  instructors: [
    {
      id: string,
      familyName: string,
      givenName: string
    }
  ],
  status: string
}

interface OneRosterClass {
  title: string,
  classCode: string,
  status: string,
  url?: URL
}
@Component({
  selector: 'app-courses-elearning',
  templateUrl: './courses-elearning.component.html',
  styleUrls: []
})
export class CoursesElearningComponent implements OnInit, OnDestroy{
  public selectedClass: any;
  public isLoading = true;        // Only if data is loaded
  public oneRosterEnabled: boolean = false;
  public mainClass: boolean = false;
  public mergedClassGroup: CourseMerge[];
  public classDivisions: CourseDivision[];
  public divisionInstructors: string[] = [];
  public supervisor: boolean = false;
  public complexCourse: boolean = false;
  public sections: boolean = false;
  public currentClass: boolean = false;
  public enableActionsSetting: boolean = false;
  public oneRosterClasses: OneRosterClass[];
  private routeParams: any;
  private fragmentSubscription?: Subscription;
  private paramsSubscription?: Subscription;

  constructor(private coursesService: CoursesService,
              private errorService: ErrorService,
              private loadingService: LoadingService,
              private route: ActivatedRoute,
              private router: Router,
              private _profileService: ProfileService,
              private _diagnosticsService: DiagnosticsService,
              private gradeScaleService: GradeScaleService,
              private _context: AngularDataContext,
              private _configurationService: ConfigurationService) {
  }

  ngOnInit() {
    this.enableActionsSetting = this._configurationService.settings.app['oneRosterActionsEnabled'] === true;

    this.paramsSubscription = this.route.parent?.params.subscribe(routeParams => {
      this.routeParams = routeParams;
      if (!this._configurationService.settings.app['oneRosterDisabled']) {
        this.loadData();
      } else {
        this.router.navigate(['courses', this.routeParams.course, this.routeParams.year, this.routeParams.period, 'details']);
      }
    });

    this.fragmentSubscription = this.route.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.loadData();
      }
    });

    this._diagnosticsService.hasService('OneRosterService').then((result) => {
      this.oneRosterEnabled = result;
    }).catch(err => {
      console.error(err);
    });
  }

  loadData() {
    // show loading
    this.loadingService.showLoading();

    const { course, year, period } = this.routeParams;

    this.coursesService.getCourseClass(course, year, period)
      .then(async courseClass => {
        // set selected course class
        this.selectedClass = courseClass;
        if (this.oneRosterEnabled) {
          // get instructor id
          const instructor = await this._context.model('instructors/me').select('id').getItem();
          // filter the current instructor and get the role
          const role = this.selectedClass.instructors.filter(element => element.instructor.id === instructor.id).map(element => element.role.alternateName)[0];
          this.supervisor = role === 'supervisor' ? true : false;
          this.complexCourse = this.selectedClass.course?.courseStructureType === 4 ? true : false;
          this.sections = this.selectedClass.sections?.length > 0;
          if (this.selectedClass.department.currentYear === this.selectedClass.year.id &&
            this.selectedClass.department.currentPeriod === this.selectedClass.period.id) {
            this.currentClass = true;
          } else {
            this.currentClass = false;
          }
          this.getClassOneRosterGroup();
          this.getClassOneRosterDivisions();
          this.getAllOneRosterClasses();
        }

        // hide loading
        this.loadingService.hideLoading();
        this.isLoading = false;
      })
      .catch(err => {
        // hide loading
        this.loadingService.hideLoading();
        return this.errorService.navigateToError(err);
      });
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

  async getClassOneRosterGroup(): Promise<void> {
    try {
      if (this.selectedClass) {
        this.mergedClassGroup = await this._context
          .model('instructors/me/classes/oneroster/replacements')
          .select('id, replacedBy, courseClass')
          .expand('replacedBy($select=id,title,course/displayCode as displayCode,year/alternateName as yearName, period/name as periodName, department/name as departmentName), courseClass($select=id,title,course/displayCode as displayCode,year/alternateName as yearName, period/name as periodName, department/name as departmentName)')
          .filter(`replacedBy eq '${this.selectedClass.id}' or courseClass eq '${this.selectedClass.id}'`)
          .orderBy('courseClass/title')
          .getItems();

        if (this.mergedClassGroup && this.mergedClassGroup[0]?.replacedBy?.id === this.selectedClass?.id.toString()) {
          this.mainClass = true;
        }
      }
    } catch (err) {
      console.error(err)
    }
  }

  async getClassOneRosterDivisions(): Promise<void> {
    try {
      if (this.selectedClass) {
        this.classDivisions = await this._context
          .model(`instructors/me/classes/${this.selectedClass.id}/oneroster/classes`)
          .select('id, title, instructors, status')
          .expand('instructors($select=id,familyName,givenName)')
          .where('status').equal('active')
          .orderBy('title')
          .getItems();

        this.divisionInstructors = [];
        for (const division of this.classDivisions) {
          let instructors = division.instructors.map(item => `${item.familyName} ${item.givenName}`).join(", ")
          this.divisionInstructors.push(instructors);
        }
      }
    } catch (err) {
      console.error(err)
    }
  }

  async removeCourseMerge(id: string): Promise<void> {
    try {
      if (id && this.selectedClass?.id) {
        await this._context
          .model('instructors/me/classes/oneroster/replacements')
          .remove({
            courseClass: id.toString(),
            replacedBy: this.selectedClass.id.toString()
          });

        this.getClassOneRosterGroup();
      }
    } catch (err) {
      console.error(err)
    }
  }

  async getAllOneRosterClasses(): Promise<void> {
    try {
      if (this.selectedClass) {
        this.oneRosterClasses = await this._context
          .model(`instructors/me/classes/${this.selectedClass.id}/oneroster/descendants`)
          .getItems();

        let replacedBy = undefined;
        if(!this.mainClass && this.mergedClassGroup.length > 0) {
          replacedBy = this.mergedClassGroup[0].replacedBy.id;
        }

        this.oneRosterClasses.map(async element => {
          // consider classes that are not inactive or already active as active
          if (element.status !== 'inactive' && element.status !== 'active') {
            element.status = 'active'
          }

          // find the url of the oneRosterClass through the NavigationLinks of the class or class sections
          const findMatchingUrl = (links: { alternateName: string, url: string }[]) => {
            for (const link of links) {
              if (element.classCode === link.alternateName) {
                return link.url;
              }
            }
            return null;
          };

          if (!this.mainClass && replacedBy === element.classCode) {
            const replacedByCourse = await this._context
              .model('instructors/me/classes')
              .where('id')
              .equal(replacedBy)
              .expand('navigationLinks')
              .getItem();

            if (replacedByCourse) {
              element.url = replacedByCourse.navigationLinks.filter(element => element.alternateName === replacedBy)[0].url;
            }
          } else {
            element.url = findMatchingUrl(this.selectedClass.navigationLinks)
              || this.selectedClass.sections
                .map(section => findMatchingUrl(section.navigationLinks))
                .find(url => url);
          }
        })
      }
    } catch (err) {
      console.error(err)
    }
  }
}
