import { AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastService } from '@universis/common';
import { AdvancedFormComponent } from '@universis/forms';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { RouterModalOkCancel } from '@universis/common/routing';

export declare type FormioAnySubmissionCallback = (error: any, submission: object) => void;

@Component({
  selector: 'universis-course-external-links-edit',
  templateUrl: './external-links-edit.component.html'
})
export class SectionExternalLinksEditComponent extends RouterModalOkCancel implements AfterViewInit, OnDestroy, OnInit {
  @Input() continueLink: any;
  @Input() courseClass: any;
  @Input() studyGuideUrl?: URL | string;
  @Input() eLearningUrl?: URL | string;
  @Input() courseClassId?: number | string;

  @ViewChild('formEditGeneral') formEditGeneral?: AdvancedFormComponent;
  private formLoadSubscription?: Subscription | undefined;
  public srcEditGeneral = 'sectionExternalLinks';
  public sectionId: any;
  public data: any;
  currentInstuctor: any;
  othersNavigationLinks: any;
  removedNavigationLinks: any;
  formChangeSubscription: any;


  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    private _context: AngularDataContext,
    private translate: TranslateService,
    private toastService: ToastService,
    private _activatedRoute: ActivatedRoute) {
    super(router, route);
    this.modalClass = 'modal-lg';
  }

  async ngOnInit() {
    this.okButtonClass = "btn btn-primary";

    this._activatedRoute.params.subscribe((params: any) => {
      this.courseClassId = params['id'];
      this.sectionId = params['sectionId'];
    })

    this.currentInstuctor = await this._context.model('instructors/me').asQueryable().select('user').getItem();
    this.data = await this._context.model('instructors/me/classes').asQueryable()
      .expand('locales,course($expand=locales),sections($expand=navigationLinks)')
      .where('id').equal(this.courseClassId).getItem().then(course => { return course.sections.find(section => section.id == this.sectionId) });

    //keep navigation links created by other users
    this.removedNavigationLinks = this.data.navigationLinks.filter((navigationLink: any) => navigationLink.createdBy != this.currentInstuctor['user']);

    //keep only id, name and url
    this.othersNavigationLinks = this.removedNavigationLinks.map((item: any) => {
      return {
        id: item.id,
        name: item.name,
        url: item.url
      }
    });

    //add others navigation links to data
    this.data.othersNavigationLinks = this.othersNavigationLinks;
    
    //add to navigationLinks only navigationLinks created by current user
    this.data.navigationLinks = this.data.navigationLinks.filter((navigationLink: any) => navigationLink.createdBy == this.currentInstuctor['user']);

  }

  async ngAfterViewInit() {
    if (this.formEditGeneral && this.formEditGeneral.form) {
      this.formLoadSubscription = this.formEditGeneral.form.formLoad.subscribe(async () => {
        if (this.formEditGeneral?.form?.options) {
          // hide formio alerts errors
          //this.formEditGeneral.form.options.disableAlerts = true;
        }
      });

      this.formChangeSubscription = this.formEditGeneral.form.change.subscribe((event: {
        changed: any,
        isValid: boolean
      }) => {
        if (Object.prototype.hasOwnProperty.call(event, 'isValid') && event.changed != null) {
          // enable or disable button based
          this.okButtonDisabled = !event.isValid;
        }
      });
    }
  }

  refreshFormioData() {
    this.formEditGeneral?.refreshForm.emit({
      submission: {
        data: this.data
      }
    });
  }

  ngOnDestroy(): void {
    if (this.formLoadSubscription) {
      this.formLoadSubscription.unsubscribe();
    }
  }

  async ok(): Promise<any> {
    try {
      const formioData = this.formEditGeneral.form.formio.data;
      let navigationLinks = formioData.navigationLinks.map((item: any) => {
        if (item.id) {
          return {
            id: item.id,
            name: item.name,
            url: item.url
          }
        }
        else {
          return {
            name: item.name,
            url: item.url
          }
        }
      });

      //drop empty navigationLinks from array
      navigationLinks = navigationLinks.filter(link => link.url !== "" && link.name !== "");

      //add removed navigation links from others users
      if (this.othersNavigationLinks[0].url != null) { 
        navigationLinks.push(...this.othersNavigationLinks);
      }
      
      //save navigation links to server
      await this._context.model(`instructors/me/classes/${this.courseClassId}/sections/${this.sectionId}/navigationLinks`)
        .save({
          navigationLinks: navigationLinks
        });
      // show success toast message
      this.toastService.show(
        this.translate.instant('ExternalLinksModal.SuccessTitle'),
        this.translate.instant(('ExternalLinksModal.SuccessMessage'))
      );
      // close modal
      this.close({
        fragment: 'reload',
        skipLocationChange: true
      });
    } catch (err) {
      console.log(err)
      // show fail toast message
      if (err.status === 403) {
        this.toastService.show(
          this.translate.instant('ExternalLinksModal.FailTitle'),
          this.translate.instant(('ExternalLinksModal.InsufficientPermission'))
        );
      } else {
        this.toastService.show(
          this.translate.instant('ExternalLinksModal.FailTitle'),
          this.translate.instant(('ExternalLinksModal.FailMessage'))
        );
      }
      // close modal without triggering a reload
      this.cancel();
    }
  }

  cancel(): Promise<any> {
    return this.close({ skipLocationChange: true });
  }

  onChange(event: { changed?: any; data?: any; isValid: boolean }, item: any) {
    if (this.formEditGeneral.form.formio.checkValidity()) {
      this.okButtonDisabled = false
    } else {
      this.okButtonDisabled = true;
    }
  }
}
