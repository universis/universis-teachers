import { Component, OnInit, OnDestroy } from '@angular/core';
import { CoursesService } from '../../services/courses.service';
import {ActivatedRoute} from '@angular/router';
import {ConfigurationService, DiagnosticsService, ErrorService, GradeScaleService} from '@universis/common';
import {LoadingService} from '@universis/common';
import {template} from 'lodash';
import {ProfileService} from '../../../profile/services/profile.service';
import { Subscription } from 'rxjs';
import { AngularDataContext } from '@themost/angular';


@Component({
  selector: 'app-courses-details-general',
  templateUrl: './courses-details-general.component.html',
  styleUrls: ['./courses-details.component.scss']
})
export class CoursesDetailsGeneralComponent implements OnInit, OnDestroy {
  public selectedClass: any;
  public isLoading = true;        // Only if data is loaded
  public gradeScale: any;
  public currentInstructor: any;
  private routeParams: any;
  public hideElearningUrl: boolean = false;
  private fragmentSubscription?: Subscription;
  private paramsSubscription?: Subscription;
  public supportBooksService: any;
  //public courseClass: any;

  constructor(private coursesService: CoursesService,
              private errorService: ErrorService,
              private loadingService: LoadingService,
              private route: ActivatedRoute,
              private _profileService: ProfileService,
              private _diagnosticsService: DiagnosticsService,
              private gradeScaleService: GradeScaleService,
              private _context: AngularDataContext,
              private _configurationService: ConfigurationService) {
  }

  async ngOnInit() {
    this.currentInstructor = await this._profileService.getInstructor();
    this.hideElearningUrl = this._configurationService.settings.app['HideElearningUrl'] === true;
    this._diagnosticsService.hasService('EudoxusService').then((result) => {
      this.supportBooksService = result;
    }).catch(err => {
      console.log(err);
    });

    this.paramsSubscription = this.route.parent?.params.subscribe(routeParams => {
      this.routeParams = routeParams;
      this.loadData();
    });

    this.fragmentSubscription = this.route.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.loadData();
      }
    });
  }

  loadData() {
    // show loading
    this.loadingService.showLoading();

    const { course, year, period } = this.routeParams;

    this.coursesService.getCourseClass(course, year, period)
      .then(async courseClass => {
        // set selected course class
        this.selectedClass = courseClass;
        return this.gradeScaleService.getGradeScale(courseClass.course.gradeScale);
      })
      .then(gradeScale => {
        this.gradeScale = gradeScale;
        // get class and elearning url from instructor/department/organization/instituteConfiguration
        return this._profileService.getInstructor();
      })
      .then(instructor => {
        if (instructor && instructor.department && instructor.department.organization &&
          instructor.department.organization.instituteConfiguration) {
          const instituteConfig = instructor.department.organization.instituteConfiguration;

          if (this.selectedClass.statistic?.studyGuideUrl) {
            this.selectedClass.classUrl = this.selectedClass.statistic.studyGuideUrl;
          } else if (instituteConfig.courseClassUrlTemplate) {
            this.selectedClass.classUrl = template(instituteConfig.courseClassUrlTemplate)(this.selectedClass);
          } else {
            this.selectedClass.classUrl = '';
          }

          if (this.selectedClass.statistic?.eLearningUrl) {
            this.selectedClass.eLearningUrl = this.selectedClass.statistic.eLearningUrl;
          } else if (instituteConfig.eLearningUrlTemplate) {
            this.selectedClass.eLearningUrl = template(instituteConfig.eLearningUrlTemplate)(this.selectedClass);
          } else {
            this.selectedClass.eLearningUrl = '';
          }
        }

        // hide loading
        this.loadingService.hideLoading();
        this.isLoading = false;
      })
      .catch(err => {
        // hide loading
        this.loadingService.hideLoading();
        return this.errorService.navigateToError(err);
      });
  }

  public openUrl(url) {
    if (url) {
      window.open(url, '_blank');
    }
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

  isLoadingBooksHandler(event: boolean) {
    if (event === false) {
      this.loadingService.hideLoading();
      this.isLoading = false;
    } else {
      this.loadingService.showLoading();
    }
  }

  checkIfInstructor(instructors: Array<any>): boolean {
    const there=instructors.find(instructor => instructor.instructor == this.currentInstructor.id);
    if (there!=undefined) {
      return true
    }else{
      return false
    }
  }
}
