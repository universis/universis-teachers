import {TableConfiguration} from '@universis/ngx-tables';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

import * as _config_current from './theses-advanced-table.config.current.json';
import * as _config_all from './theses-advanced-table.config.all.json';
import * as _config_list from './theses-advanced-table.config.list.json';

import * as _search_current from './theses-advanced-table.search.current.json';
import * as _search_all from './theses-advanced-table.search.all.json';
import * as _search_list from './theses-advanced-table.search.list.json';

export class ThesesAdvancedTableConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration | any {
        try {
            if (route.params['list'] == "current")
                return _config_current
            else (route.params['list'] == "all")
            return _config_all
        } catch (err) {
            return _config_list
        }
    }
}

export class ThesesAdvancedTableSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration | any {
        try {
            if (route.params['list'] == "current")
                return _search_current
            else (route.params['list'] == "all")
            return _search_all
        } catch (err) {
            return _search_list
        }
    }
}

export class ThesesAdvancedDefaultTableConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
       return _config_list;
    }
}
