import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MostModule } from '@themost/angular';
import { RouterTestingModule } from '@angular/router/testing';
import { AngularDataContext } from '@themost/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { InstructorRecentUploadsComponent } from './instructor-recent-uploads.component';
import { ConfigurationService } from '@universis/common';
import { TranslateModule } from '@ngx-translate/core';
import { TestingConfigurationService } from './../../../test';
import { CoursesService } from './../../../courses/services/courses.service';
import { SharedModule } from '@universis/common';

const mockCourse1 = {
  object: {
    id: 1,
    name: 'myname',
    course: {
      id: 1,
      displayCode: 'myCode'
    },
    examPeriod: {
      name: 'exam_period'
    },
    year: {
      id: 2019,
      alternateName: 'YrAltName'
    },
    status: {
      alternateName: 'statAltName'
    },
    classes: [{
      courseClass: {
        period: 2
      }
    }]
  },
  actionStatus: {
    dateCreated: new Date()
  },
};

const mockCourse2 = {
  object: {
    id: 2,
    name: 'myname2',
    course: {
      id: 2,
      displayCode: 'myCode2'
    },
    examPeriod: {
      name: 'exam_period2'
    },
    year: {
      id: 20192,
      alternateName: 'YrAltName2'
    },
    status: {
      alternateName: 'statAltName2'
    },
    classes: [{
      courseClass: {
        period: 22
      }
    }]
  },
  actionStatus: {
    dateModified: new Date()
  },
};

describe('InstructorRecentUploadsComponent', () => {
  let component: InstructorRecentUploadsComponent;
  let fixture: ComponentFixture<InstructorRecentUploadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        RouterTestingModule,
        HttpClientTestingModule,
        SharedModule.forRoot(),
        MostModule.forRoot({
          base: '/',
          options: {
            useMediaTypeExtensions: false
          }
        })
      ],
      declarations: [ InstructorRecentUploadsComponent ],
      providers: [
        {
          provide: ConfigurationService,
          useClass: TestingConfigurationService
        },
        AngularDataContext
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructorRecentUploadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have loading set to true by default', () => {
    expect(component.isLoading).toEqual(true);
  });

  describe('given data received from the back end', () => {
    let coursesService;
    let spy;

    beforeEach(() => {
      coursesService = TestBed.get(CoursesService);
      spy = coursesService.getUploadHistoryRecent =
        jasmine.createSpy('getUploadHistoryRecent').and.returnValue(
          Promise.resolve([mockCourse1])
        );
    });


    it('should populate the actions list', async() => {
      await component.loadData();
      expect(component.actions).toBeTruthy();
    });

    it('should produce the correct URL', async() =>{
      await component.loadData();
      const mockURL = '/courses/1/2019/2/exams/1';
      const url = component.getUrl(component.actions[0]);

      expect(url.join('/')).toEqual(mockURL);

    });

    it('should update the isLoading after data fetching', async() => {
      expect(component.isLoading).toEqual(true);
      await component.loadData();
      expect(component.isLoading).toEqual(false);
    });

    it('should be able to handle multiple records', async() => {

      spy = coursesService.getUploadHistoryRecent =
        jasmine.createSpy('getUploadHistoryRecent').and.returnValue(
          Promise.resolve([mockCourse1, mockCourse2])
        );

      await component.loadData();
      expect(component.actions.length).toEqual(2);
      expect(component.getUrl(component.actions[0]))
        .not
        .toEqual(component.getUrl(component.actions[1])
      );
    });

    describe('should sort the courses by date', () => {
      it('when they are already sorted', async() => {
        const mock1 = {
          ...mockCourse1,
          object: {
            ...mockCourse1.object,
            classes: [
              {courseClass: {period: 3}},
              {courseClass: {period: 2}},
              {courseClass: {period: 1}},
            ]
          }
        };

        mock1.actionStatus.dateCreated.setDate(new Date().getDate() + 1);

        const expectedId = 3;
        const expectedUrl = `/courses/1/2019/${expectedId}/exams/1`;

        spy = coursesService.getUploadHistoryRecent =
          jasmine.createSpy('getUploadHistoryRecent').and.returnValue(
            Promise.resolve([mock1])
          );

        await component.loadData();

        const realUrl = component.getUrl(component.actions[0]).join('/');
        expect(realUrl).toEqual(expectedUrl);
      });

      it('when they are not sorted', async() => {
        const mock1 = {
          ...mockCourse1,
          object: {
            ...mockCourse1.object,
            classes: [
              {courseClass: {period: 2}},
              {courseClass: {period: 3}},
              {courseClass: {period: 1}},
            ]
          }
        };

        mock1.actionStatus.dateCreated.setDate(new Date().getDate() + 1);

        const expectedId = 3;
        const expectedUrl = `/courses/1/2019/${expectedId}/exams/1`;

        spy = coursesService.getUploadHistoryRecent =
          jasmine.createSpy('getUploadHistoryRecent').and.returnValue(
            Promise.resolve([mock1])
          );

        await component.loadData();

        const realUrl = component.getUrl(component.actions[0]).join('/');
        expect(realUrl).toEqual(expectedUrl);
      });
    });

    it('should return empty array when there are no classes', async() => {
      const mock1 = {
        ...mockCourse1,
        object: {
          ...mockCourse1.object,
          classes: []
        }
      };

      spy = coursesService.getUploadHistoryRecent =
        jasmine.createSpy('getUploadHistoryRecent').and.returnValue(
          Promise.resolve([mock1])
        );

      await component.loadData();

      const realUrl = component.getUrl(component.actions[0]);
      expect(realUrl.length).toEqual(0);
    });

    describe('When there is an upload action without the corseExam', () => {
      const mockCourse3 = {
        object: {
          id: 1,
          name: 'myname',
          course: {
            id: 1,
            displayCode: 'myCode'
          },
          examPeriod: {
            name: 'exam_period'
          },
          year: {
            id: 2019,
            alternateName: 'YrAltName'
          },
          status: {
            alternateName: 'statAltName'
          },
          classes: [{
            courseClass: {
              period: 2
            }
          }]
        },
        actionStatus: {
          dateCreated: new Date()
        },
      };

      const mockCourse4 = {
        object: undefined,
        actionStatus: {
          dateModified: new Date()
        },
      };

      beforeEach(() => {
        spy = coursesService.getUploadHistoryRecent =
        jasmine.createSpy('getUploadHistoryRecent').and.returnValue(
          Promise.resolve([mockCourse3, mockCourse4])
        );
      });

      it('should exclude the actions without courseExam from the actions array', async() => {
        await component.loadData();
        expect(component.actions.length).toBe(1);
      });

      describe('the getURL function', () => {
        it('should return an empty array', async() => {
          await component.loadData();
          const url = component.getUrl(mockCourse4);
          expect(url && url.length === 0).toBe(true);
        });
      });
    });

    describe('When all upload actions have undefine course exam', () => {
      const mockCourse5 = {
        object: undefined,
        actionStatus: {
          dateCreated: new Date()
        },
      };

      const mockCourse6 = {
        object: undefined,
        actionStatus: {
          dateModified: new Date()
        },
      };

      beforeEach(() => {
        spy = coursesService.getUploadHistoryRecent =
        jasmine.createSpy('getUploadHistoryRecent').and.returnValue(
          Promise.resolve([mockCourse5, mockCourse6])
        );
      });

      it('should exclude all upload actions', async() => {
        await component.loadData();
        expect(component.actions.length).toBe(0);
      });

      describe('The UI ', () => {
        it ('should show there are not available upload actions', async() => {
          try {
            await component.loadData();
            fixture.detectChanges();
            const textInUI = fixture.nativeElement.querySelector('.info-text').textContent;
            expect(textInUI).toEqual('iDashboard.gradeSubmissionNoRecord');
          } catch (err) {
            fail(err);
          }
        });
      });
    });
  });
});
