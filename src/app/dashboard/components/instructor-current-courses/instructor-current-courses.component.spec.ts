import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructorCurrentCoursesComponent } from './instructor-current-courses.component';
import { ConfigurationService } from '@universis/common';
import { TestingConfigurationService } from 'src/app/test';
import { MostModule } from '@themost/angular';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { ErrorService } from '@universis/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SharedModule } from '@universis/common';

describe('InstructorCurrentCoursesComponent', () => {
  let component: InstructorCurrentCoursesComponent;
  let fixture: ComponentFixture<InstructorCurrentCoursesComponent>;

  const errorSvc = jasmine.createSpyObj('ErrorService,', ['getLastError', 'navigateToError', 'setLastError']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        SharedModule.forRoot(),
        RouterTestingModule,
        MostModule.forRoot({
          base: '/',
          options: {
              useMediaTypeExtensions: false
          }
        }),
        TranslateModule.forRoot(),
        HttpClientTestingModule
      ],
      declarations: [ InstructorCurrentCoursesComponent ],
      providers: [
        {
          provide: ConfigurationService,
          useClass: TestingConfigurationService
        },
        {
          provide: ErrorService,
          useValue: errorSvc
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructorCurrentCoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
