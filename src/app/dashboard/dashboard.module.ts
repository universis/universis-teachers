import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { NgChartsModule } from 'ng2-charts';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { MostModule } from '@themost/angular';
import { CommonModule } from '@angular/common';
import { InstructorOpenGradesComponent } from './components/instructor-open-grades/instructor-open-grades.component';
import { InstructorCurrentCoursesComponent } from './components/instructor-current-courses/instructor-current-courses.component';
import { environment } from '../../environments/environment';
import { NgPipesModule } from 'ngx-pipes';
import { CoursesSharedModule } from '../courses/courses-shared.module';
import { InstructorRecentUploadsComponent } from './components/instructor-recent-uploads/instructor-recent-uploads.component';
import { SharedModule } from '@universis/common';
import { EventsModule } from '@universis/ngx-events';
import { QaModule } from '@universis/ngx-qa';

import * as el from "./i18n/dashboard.el.json"
import * as en from "./i18n/dashboard.en.json"

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    CoursesSharedModule,
    NgChartsModule,
    TranslateModule,
    MostModule,
    NgPipesModule,
    SharedModule,
    EventsModule,
    QaModule
  ],
  declarations: [
    DashboardComponent,
    InstructorOpenGradesComponent,
    InstructorCurrentCoursesComponent,
    InstructorRecentUploadsComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DashboardModule {
  constructor(private _translateService: TranslateService) {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
  }
}
