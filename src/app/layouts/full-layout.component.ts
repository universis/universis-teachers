import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { AppSidebarService } from '../teachers-shared/services/app-sidebar.service';
import { UserService, ApplicationSettingsConfiguration, DiagnosticsService, asyncMemoize } from '@universis/common';
import { ConfigurationService } from '@universis/common';
import { ApplicationSettings } from "../teachers-shared/teachers-shared.module";
import { NavigationEnd, Router } from '@angular/router';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import ISO6391 from 'iso-639-1';
import { MessagesService } from '../messages/services/messages.service';
import { MessageSharedService } from '../teachers-shared/services/messages.service';
import { environment } from '../../environments/environment';

// LOCALES: import extra locales here
import * as el from '../messages/i18n/messages.el.json';
import * as en from '../messages/i18n/messages.en.json';

@Component({
  selector: 'app-layouts',
  templateUrl: './full-layout.component.html',
  styles: [`
    button.dropdown-item {
      cursor: pointer;
    }
    .inner__content {
      max-height: 70vh;
      overflow-y: auto;
    }
    `],
  providers: [MessagesService]
})

export class FullLayoutComponent implements OnInit {


  constructor(private _context: AngularDataContext,
    private _appSidebar: AppSidebarService,
    private _configurationService: ConfigurationService,
    private _userService: UserService,
    private _router: Router,
    private notificationsMsgService: MessagesService,
    private messageSharedService: MessageSharedService,
    private _diagnosticsService: DiagnosticsService,
    private _translateService: TranslateService,) {
    this._router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        this.buildLanguages(event.urlAfterRedirects.toString());
        this.navItems = this._appSidebar.navigationItems;
      }
    });
    this.messageSharedService.getAction().subscribe(async message => {
      this.numOfMsg = await this.notificationsMsgService.getCountOfUnreadMessages();
    });
  }
 @ViewChild('appSidebarNav') appSidebarNav: any;
  public status = { isOpen: false };
  public user;
  public currentLang;
  public languages: any = [];
  public lang: any = {};
  public navItems: any = [];
  public logoPath;
  public navigationLinks;
  public messages: any;
  public waitForMessages = false;
  public take = 4;
  public skip = 0;
  public numOfMsg = [
    {
      total: 0
    }
  ];
  public header;
  public supportQA = false;
  public hasSignRoles: boolean = false;

  async ngOnInit() {
    this._diagnosticsService.hasService('QualityAssuranceService').then((result) => {
      this.supportQA = result;
    }).catch(err => {
      console.log(err);
    });
    // get user sign roles
    this.hasReportSignRoles()
      .then((hasSignRoles: boolean) => {
        this.hasSignRoles = hasSignRoles;
      })
      .catch((err) => {
        console.log(err);
      });
    // get current user
    this._userService.getUser().then(async user => {
      this.user = user;
      // get current language
      this.currentLang = ISO6391.getNativeName(this._configurationService.currentLocale);
      // get count of messages
      this.numOfMsg = await this.notificationsMsgService.getCountOfUnreadMessages();
      const headers = this._configurationService.settings.app && (<ApplicationSettings>this._configurationService.settings.app).header;
      if (Array.isArray(headers)) {
        const header = headers.find(x => {
          return x.additionalType === 'Header' && x.inLanguage === this._configurationService.currentLocale;
        });
        if (header && header.body && header.body.trim().length) {
          this.header = header;
        }
      }
      // get languages
      this.languages = this._configurationService.settings?.i18n?.locales;
      // get path of brand logo
      this.logoPath = this._configurationService.settings.app?.image;
      // get Navigation Links Collection for current Language
      this.navigationLinks = ((<any>this._configurationService.settings.app).navigationLinks || []).filter(x => {
        return x.inLanguage === this._translateService.currentLang;
      });
      // build languages
      this.buildLanguages(this._router.url.toString());
      // set navigation items
      this.navItems = this._appSidebar.navigationItems;
      for (let i = 0; i < this.languages.length; i++) {
        this.languages[i] = ISO6391.getNativeName(this.languages[i]);
        this._translateService.onLangChange.subscribe((event: LangChangeEvent) => {
          window.location.reload();
        });
      }
    });
  }
  async getMessages() {

    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);

    this.waitForMessages = true;
    try {
      this.messages = (await this.notificationsMsgService.getUnreadMessages(this.take, this.skip)).value;
    } catch (e) {
      console.log(e);
    } finally {
      this.waitForMessages = false;
    }
  }

  buildLanguages(url) {
    const findLang = this._appSidebar.navigationItems.find(x => {
      return x.url === '/lang';
    });
    if (findLang && findLang.children?.length === 0) {
      for (let i = 0, x = 0; i < (this._configurationService.settings?.i18n?.locales as any).length; i++, x = x + 5) {
        this.lang = {};
        this.lang.name = ISO6391.getNativeName((this._configurationService.settings?.i18n?.locales as any)[i]);
        this.lang.url = '/lang/'.concat(ISO6391.getCode(this.lang.name)).concat('/').concat(url);
        this.lang.icon = 'icon-cursor';
        this.lang.index = x;
        findLang.children.push(this.lang);
        if ((this._configurationService.settings?.i18n?.locales as any).length < findLang.children.length) {
          findLang.children.splice(0, (this._configurationService.settings?.i18n?.locales as any).length);
        }
      }
    }
  }

  changeLanguage(lang) {
    // auto-reloads on language change
    this._configurationService.currentLocale = (ISO6391.getCode(lang));
  }

  toggleSidebarVisibility(): void {
    const body: HTMLBodyElement = document.querySelector('body');
    if (body.classList.contains('sidebar-show')) {
      body.classList.remove('sidebar-show');
    } else {
      body.classList.add('sidebar-show');
    }
  }

  @asyncMemoize()
  hasReportSignRoles(): Promise<boolean> {
    return this._context
      .model('instructors/me/hasSignRoles')
      .asQueryable()
      .getItem();
  }
}

