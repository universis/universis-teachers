import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SendMessageToExamParticipantsComponent } from './send-message-to-exam-participants.component';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ModalModule, BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { MostModule } from '@themost/angular';
import { LoadingService, ModalService, ErrorService, ToastService, ConfigurationService } from '@universis/common';
import { MessagesService } from '../../services/messages.service';
import { MessageSharedService } from 'src/app/teachers-shared/services/messages.service';
import { TestingConfigurationService } from '@universis/common/testing';

describe('SendMessageToExamParticipantsComponent', () => {
  let component: SendMessageToExamParticipantsComponent;
  let fixture: ComponentFixture<SendMessageToExamParticipantsComponent>;

  const loadingSvc = jasmine.createSpyObj('LoadingService', ['showLoading', 'hideLoading']);
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        FormsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        ModalModule.forRoot(),
        MostModule.forRoot({
          base: '/api/',
          options: {
            useMediaTypeExtensions: false,
            useResponseConversion: true
          }
        })
      ],
      declarations: [ SendMessageToExamParticipantsComponent ],
      providers: [
        BsModalService,
        BsModalRef,
        ToastService,
        {
          provide: ConfigurationService,
          useClass: TestingConfigurationService
        },
        MessagesService,
        MessageSharedService,
        {
          provide: LoadingService,
          useValue: loadingSvc
        },
        ModalService,
        ErrorService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendMessageToExamParticipantsComponent);
    component = fixture.componentInstance;
    component.instructor = {
      email: 'test@example.com'
    };
    component.courseExam = {
      id: 1,
      description: 'description'
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show the message form', () => {
    const el = fixture.nativeElement;
    const form = el.querySelector('form');
    expect(form).toBeDefined();
  });

  it('should show no email message when the instructor has no email', () => {
    component.instructor = {
      id: 1
    };
    const el = fixture.nativeElement;
    fixture.detectChanges();

    const form = el.querySelector('input');
    expect(form).toBeFalsy();

    const errorMessage = el.querySelector('.text-danger');
    expect(errorMessage).toBeDefined();
  });
});
