import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {FormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import { MessagesListComponent } from './messages-list.component';
import {MessagesService} from '../../services/messages.service';
import {RouterTestingModule} from '@angular/router/testing';
import {MostModule} from '@themost/angular';
import {ConfigurationService, LoadingService, SharedModule} from '@universis/common';
import {ApiTestingController, ApiTestingModule, TestingConfigurationService} from '@universis/common/testing';
import {MessageSharedService} from '../../../teachers-shared/services/messages.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NgPipesModule} from 'ngx-pipes';
import {ModalModule} from 'ngx-bootstrap/modal';
import {APP_BASE_HREF} from '@angular/common';

describe('MessagesListComponent', () => {
  let component: MessagesListComponent;
  let fixture: ComponentFixture<MessagesListComponent>;
  let mockApi: ApiTestingController;
  const loadingSvc = jasmine.createSpyObj('LoadingService', ['showLoading' , 'hideLoading' ]);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessagesListComponent ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        FormsModule,
        TranslateModule.forRoot(),
        InfiniteScrollModule,
        MostModule.forRoot({
          base: '/api/',
          options: {
            useMediaTypeExtensions: false
          }
        }),
        SharedModule.forRoot(),
        NgPipesModule,
        ModalModule.forRoot(),
        ApiTestingModule.forRoot(),
      ],
      providers: [
        MessagesService,
        MessageSharedService,
        {
          provide: ConfigurationService,
          useClass: TestingConfigurationService
        },
        {
          provide: LoadingService,
          useValue: loadingSvc
        },
        {
          provide: APP_BASE_HREF,
          useValue: '/'
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    mockApi = TestBed.get(ApiTestingController);
  });

  it('should create', () => {
    mockApi.match({
      url: '/api/instructors/me/messages/',
      method: 'GET'
    }).map(request => {
      request.flush({
        value: []
      });
    });
    expect(component).toBeTruthy();
  });
});
