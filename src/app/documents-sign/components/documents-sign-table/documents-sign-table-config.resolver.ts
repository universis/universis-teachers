import {TableConfiguration} from '@universis/ngx-tables';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

import * as _config_active from './documents-sign-table.config.active.json';
import * as _config_all from './documents-sign-table.config.all.json';
import * as _config_list from './documents-sign-table.config.list.json';

import * as _search_active from './documents-sign-table.search.active.json';
import * as _search_all from './documents-sign-table.search.all.json';
import * as _search_list from './documents-sign-table.search.list.json';

export class DocumentsSignTableConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration | any{
        try {
            if (route.params['list'] == "active")
                return _config_active
            else (route.params['list'] == "all")
            return _config_all
        } catch (err) {
            return _config_list
        }
    }
}

export class DocumentsSignTableSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration | any{
        try {
            if (route.params['list'] == "active")
                return _search_active
            else (route.params['list'] == "all")
            return _search_all
        } catch (err) {
            return _search_list
        }
    }
}

export class DocumentsSignDefaultTableConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        //return import(`./documents-sign-table.config.list.json`);
        return _config_list
    }
}
