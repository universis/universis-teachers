import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { LoadingService,ModalService } from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-documents-sign-message',
  templateUrl: './documents-sign-message.component.html',
  styles: [],
})
export class DocumentsSignMessageComponent
  extends RouterModalOkCancel
  implements OnInit, OnDestroy {

  private subscription: Subscription;

  public result: any = {};

  constructor(
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    private translateService: TranslateService,
    private context: AngularDataContext,
    private loadingService: LoadingService,
    private modalService: ModalService,
  ) {
    // call super constructor
    super(router, activatedRoute);
    this.modalTitle = this.translateService.instant(
      'DocumentSignActions.CancelReasonLong'
    );
    this.modalClass = 'modal-lg';
  }

  ngOnInit() {
    this.cancelButtonClass = 'd-none';
    this.subscription = this.activatedRoute.params.subscribe(async (params) => {
      try {
        this.loadingService.showLoading();
        this.result = await this.context
          .model('DocumentSignActions')
          .where('id')
          .equal(params['action'])
          .and('cancelReason')
          .notEqual(null)
          .select('cancelReason')
          .getItem();
      } catch (err) {
        console.error(err);
        this.result = null;
      } finally {
        this.loadingService.hideLoading();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  async ok() {
    return this.close();
  }

  async cancel() {
    return this.close();
  }
}
