import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '@universis/common';
import {TablesModule} from '@universis/ngx-tables';
import {GenericsSharedModule} from '../generics/generics-shared.module';
import {RouterModalModule} from '@universis/common/routing';

import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {FormsModule} from '@angular/forms';
import {DocumentsSignHomeComponent} from './components/documents-sign-home/documents-sign-home.component';
import {DocumentsSignTableComponent} from './components/documents-sign-table/documents-sign-table.component';
import {
  DocumentsSignDefaultTableConfigurationResolver,
  DocumentsSignTableConfigurationResolver,
  DocumentsSignTableSearchResolver
} from './components/documents-sign-table/documents-sign-table-config.resolver';
import {ReportsSharedModule} from '@universis/ngx-reports';
import {DocumentsSignRoutingModule} from './documents-sign-routing.module';
// LOCALES: import extra locales here
import * as el from './i18n/DocumentSignActions.el.json';
import * as en from './i18n/DocumentSignActions.en.json';
import {DocumentsSignMessageComponent} from './components/documents-sign-message/documents-sign-message.component';
import {DocumentsSignRejectComponent} from './components/documents-sign-reject/documents-sign-reject.component';
import {AdvancedFormsModule} from '@universis/forms';


@NgModule({
  imports: [
    CommonModule,
    DocumentsSignRoutingModule,
    TranslateModule,
    FormsModule,
    SharedModule,
    TablesModule,
    GenericsSharedModule,
    RouterModalModule,
    ReportsSharedModule,
    AdvancedFormsModule,
  ],
  declarations: [
    DocumentsSignHomeComponent,
    DocumentsSignTableComponent,
    DocumentsSignMessageComponent,
    DocumentsSignRejectComponent
  ],
  entryComponents: [
  ],
  providers: [
    DocumentsSignDefaultTableConfigurationResolver,
    DocumentsSignTableConfigurationResolver,
    DocumentsSignTableSearchResolver,
    DocumentsSignRejectComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DocumentsSignModule {
  constructor(private _translateService: TranslateService) {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
  }
}
