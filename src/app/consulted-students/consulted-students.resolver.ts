import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { LoadingService } from '@universis/common';

@Injectable()
export class ConsultationsResolver implements Resolve<any> {
  constructor(
    private readonly context: AngularDataContext,
    private readonly translate: TranslateService,
    private readonly date: DatePipe,
    private readonly loading: LoadingService
  ) {}

  async resolve(route: ActivatedRouteSnapshot): Promise<any> {
    try {
      // show loading
      this.loading.showLoading();
      // extract student from route params
      const studentId = route.parent && route.parent.parent && route.parent.parent.params && route.parent.parent.params['studentId'];
      // get consultations
      const consultations = await this.context
        .model('instructors/me/consultedStudents')
        .asQueryable()
        .where('student')
        .equal(studentId)
        .expand('student($select=StudentSummary)')
        .orderBy('id desc')
        .take(-1)
        .getItems();
      // if there are none, exit
      if (!(Array.isArray(consultations) && consultations.length > 0)) {
        return;
      }
      // format and return the consultations in a formio-select component structure
      return Promise.resolve({
        consultations: consultations.map((item) => {
          return {
            label: `[${item.student?.studentIdentifier || '-'}] ${item.student?.familyName || '-'} ${
              item.student?.givenName || '-'
            }, ${this.translate.instant(`ConsultedStudents.Notes.Active.${item.active}`)}. ${this.translate.instant(
              'ConsultedStudents.StartDate'
            )}: ${this.date.transform(item.startDate, 'shortDate') || '-'}${
              !!item.endDate
                ? `, ${this.translate.instant('ConsultedStudents.EndDate')}: ${this.date.transform(item.endDate, 'shortDate')}`
                : ''
            }`,
            value: item.id
          };
        })
      });
    } catch (err) {
      console.error(err);
      throw err;
    } finally {
      this.loading.hideLoading();
    }
  }
}

@Injectable()
/* note: the reason this resolver is used is because AdvancedFormItemResolver
    validates the target model through the schema and instructors/me/counselorPrivateNotes does not belong to it
*/
export class CounselorPrivateNoteResolver implements Resolve<any> {
  constructor(private readonly context: AngularDataContext, private readonly loading: LoadingService) {}

  async resolve(route: ActivatedRouteSnapshot): Promise<any> {
    try {
      // show loading
      this.loading.showLoading();
      // extract note id from params
      const note: number | string = route.params && route.params['id'];
      if (note == null) {
        return;
      }
      // get target private note
      return this.context.model('instructors/me/counselorPrivateNotes').where('id').equal(note).getItem();
    } catch (err) {
      console.error(err);
      throw err;
    } finally {
      this.loading.hideLoading();
    }
  }
}
