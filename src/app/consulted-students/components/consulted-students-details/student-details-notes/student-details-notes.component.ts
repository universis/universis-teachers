import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { AppEventService, ErrorService, LoadingService } from '@universis/common';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-student-details-notes',
  templateUrl: './student-details-notes.component.html',
  styleUrls: []
})
export class StudentDetailsNotesComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  private reloadSubscription: Subscription;
  private removeSubscription: Subscription;
  public notes: any[];
  public studentId: string | number;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly loadingService: LoadingService,
    private readonly errorService: ErrorService,
    private readonly context: AngularDataContext,
    private readonly appEvent: AppEventService
  ) {}

  ngOnInit(): void {
    this.subscription = this.activatedRoute.parent.params.subscribe(async (params: { studentId: number | string }) => {
      try {
        // show loading
        this.loadingService.showLoading();
        // get student
        this.studentId = params && params.studentId;
        // fetch and display all notes for the specific student
        this.notes = await this.loadNotes();
      } catch (err) {
        console.error(err);
        this.errorService.navigateToError(err);
      } finally {
        this.loadingService.hideLoading();
      }
    });
    this.reloadSubscription = this.activatedRoute.fragment.subscribe(async (fragment) => {
      try {
        this.loadingService.showLoading();
        if (fragment === 'reload') {
          this.notes = await this.loadNotes();
        }
      } catch (err) {
        console.error(err);
        this.errorService.navigateToError(err);
      } finally {
        this.loadingService.hideLoading();
      }
    });
    this.removeSubscription = this.appEvent.removed.subscribe(async (event) => {
      try {
        this.loadingService.showLoading();
        if (
          event &&
          event.model === 'Instructors/Me/CounselorPrivateNotes' &&
          event.target &&
          !!this.notes.find((note) => note.id === event.target.id)
        ) {
          this.notes = await this.loadNotes();
        }
      } catch (err) {
        console.error(err);
        this.errorService.navigateToError(err);
      } finally {
        this.loadingService.hideLoading();
      }
    });
  }

  loadNotes(): Promise<any> {
    return this.context
      .model('instructors/me/counselorPrivateNotes')
      .asQueryable()
      .where('studentCounselor/student')
      .equal(this.studentId)
      .expand('studentCounselor($expand=student($select=StudentSummary))')
      .orderBy('pending desc, pendingDate asc')
      .take(-1)
      .getItems();
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.reloadSubscription) {
      this.reloadSubscription.unsubscribe();
    }
    if (this.removeSubscription) {
      this.removeSubscription.unsubscribe();
    }
  }
}
